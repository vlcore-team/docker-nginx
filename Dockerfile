FROM vector:7.1-bb

ADD slapt-getrc /etc/slapt-get/slapt-getrc
ADD install.sh /tmp/install
RUN [ "sh", "/tmp/install" ]

RUN [ "rm", "/tmp/install" ]
VOLUME [ "/var/www/html/" ]
VOLUME [ "/var/cache/nginx" ]
VOLUME [ "/etc/nginx/" ]

RUN [ "chmod", "+x", "/etc/rc.d/rc.nginx" ]

EXPOSE 80 443

CMD [ "/usr/sbin/nginx", "-g", "daemon off;" ]
